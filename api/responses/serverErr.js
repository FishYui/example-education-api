module.exports = function serverErr(message) {

    const req = this.req;
    const res = this.res;

    return res.status(500).json({
        success: false,
        message
    });

}