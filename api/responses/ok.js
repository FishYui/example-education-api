module.exports = function ok(response) {

    const req = this.req;
    const res = this.res;

    return res.status(200).json({
        success: true,
        response
    });

}