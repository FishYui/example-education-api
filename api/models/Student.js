module.exports = {

    attributes: {

        studentId: { //學生Id
            type: 'string',
            required: true,
            unique: true,
            columnType: 'CHAR (9)'
        },
        name: { //學生姓名
            type: 'string',
            required: true,
            columnType: 'CHAR (20)'
        },
        grade: { //學生年級
            type: 'string',
            isIn: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            defaultsTo: '1',
            columnType: 'CHAR (2)'
        },
        school_year: { //學生入學年度
            type: 'number',
            required: true,
            columnType: 'TINYINT'
        },
        department: { //學生所屬系所
            model: 'department',
        },

        courses: {
            collection: 'Course',
            via: 'student',
            through: 'Elective'
        }
        
    }
}