module.exports = {

    attributes: {

        score: {
            type: 'number',
            defaultsTo: 0,
            columnType: 'TINYINT'
        },
        student: {
            model: 'Student',
            required: true
        },
        course: {
            model: 'Course',
            required: true
        }
        
    }
}