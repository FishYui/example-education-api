module.exports = {

    attributes: {

        teacherId: { //教師Id
            type: 'string',
            required: true,
            unique: true,
            columnType: 'CHAR (9)'
        },
        name: { //教師姓名
            type: 'string',
            required: true,
            columnType: 'CHAR (20)'
        },
        school_year: { //教師就職年度
            type: 'number',
            required: true,
            columnType: 'TINYINT'
        },
        department: { //教師所屬系所
            model: 'department',
        },

        //課程關聯
        courses: {
            collection: 'Course',
            via: 'teacher'
        }
        
    }
}