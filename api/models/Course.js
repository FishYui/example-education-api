module.exports = {

    attributes: {

        courseId: { //課程Id
            type: 'string',
            required: true,
            unique: true,
            columnType: 'CHAR (6)'
        },
        name: { //課程名稱
            type: 'string',
            required: true,
            columnType: 'CHAR (20)'
        },
        grade: { //課程限制年級
            type: 'string',
            isIn: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            defaultsTo: '1',
            columnType: 'CHAR (2)'
        },
        teacher: { //授課教師
            model: 'teacher'
        },
        department: { //學生所屬系所
            model: 'department'
        },

        students: {
            collection: 'Student',
            via: 'course',
            through: 'Elective'
        }
        
    }
}