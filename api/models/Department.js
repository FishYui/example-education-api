module.exports = {

    attributes: {
        departmentId: { //系所Id
            type: 'string',
            required: true,
            unique: true,
            columnType: 'CHAR (3)'
        },
        name: { //系所名稱
            type: 'string',
            required: true,
            columnType: 'CHAR (20)'
        },

        //學生關聯
        students: {
            collection: 'Student',
            via: 'department'
        },
        //教師關聯
        teachers: {
            collection: 'Teacher',
            via: 'department'
        },
        //課程關聯
        courses: {
            collection: 'Course',
            via: 'department'
        }

    }
}