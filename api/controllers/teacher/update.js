module.exports = {

    friendlyName: 'Update',

    description: 'Update one Teacher',

    inputs: {

        teacherId: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { teacherId, name, department } = inputs;

            const teacherData = {
                name,
                department
            }

            const updateTeacher = await Teacher.updateOne({
                where: { teacherId: teacherId },
            }).set(teacherData);

            if (!updateTeacher) {
                return exits.err('E_UNKNOWN_TEACHER');
            }

            return exits.success(updateTeacher);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(errCode);
        }
    }
}