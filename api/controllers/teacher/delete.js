module.exports = {

    friendlyName: 'Delete',

    description: 'Delete one Teacher',

    inputs: {

        teacherId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { teacherId } = inputs;

            const deleteTeacher = await Teacher.destroyOne({
                where: { teacherId: teacherId }
            });

            if (!deleteTeacher) {
                return exits.err('E_UNKNOWN_TEACHER');
            }

            return exits.success();

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}