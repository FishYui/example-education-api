module.exports = {

    friendlyName: 'Find',

    description: 'Find one Teacher',

    inputs: {

        teacherId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { teacherId } = inputs;

            const findOneTeacher = await Teacher.findOne({
                where: { teacherId: teacherId }
            });

            if (!findOneTeacher) {
                return exits.err('E_UNKNOWN_TEACHER');
            }

            return exits.success(findOneTeacher);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}