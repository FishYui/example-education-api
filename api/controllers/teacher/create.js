module.exports = {

    friendlyName: 'Create',

    description: 'Create one Teacher',

    inputs: {

        name: {
            type: 'string',
            required: true
        },
        department: {
            type: 'string',
            required: true
        }
    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { name, department } = inputs;

            //入職學年度，以新增當下的民國年、月份來計算
            const date = new Date();
            const year = date.getFullYear() - 1911;
            const month = date.getMonth();
            //月份小於7則算上一年度
            const school_year = month > 7 ? year : year - 1;

            //找出同系所的最後一位老師ID，加一後為新老師ID
            const lastTeacher = await Teacher.find({
                where: { teacherId: { startsWith: `${school_year}${department}9` } },
                limit: 1,
                sort: 'id DESC',
                select: ['teacherId']
            });

            let teacherId;
            if (lastTeacher.length === 0) {
                teacherId = `${school_year}${department}901`;
            } else {
                const lastTeacherId = parseInt(lastTeacher[0].teacherId);
                teacherId = (lastTeacherId + 1).toString();
            }


            const teacherData = {
                teacherId,
                name,
                school_year,
                department
            }

            const createTeacher = await Teacher.create(teacherData).fetch();

            return exits.success(createTeacher);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}