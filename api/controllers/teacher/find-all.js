module.exports = {

    friendlyName: 'Find',

    description: 'Find all Teacher',

    inputs: {

        name: {
            type: 'string',
            required: false
        },
        school_year: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { name, school_year, department } = inputs;

            const params = {
                name,
                school_year,
                department
            }

            const findAllTeacher = await Teacher.find(params);

            return exits.success(findAllTeacher);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}