module.exports = {

    friendlyName: 'Update',

    description: 'Update one Course.',

    inputs: {

        courseId: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: false
        },
        grade: {
            type: 'string',
            required: false
        },
        teacher: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { courseId, name, grade, teacher, department } = inputs;

            const courseData = {
                name,
                grade,
                teacher,
                department
            }

            const updateCourse = await Course.updateOne({
                where: { courseId: courseId }
            }).set(courseData);

            if (!updateCourse) {
                return exits.err('E_UNKNOWN_COURSE');
            }

            return exits.success(updateCourse);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}