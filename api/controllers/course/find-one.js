module.exports = {

    friendlyName: 'Find',

    description: 'Find one Course.',

    inputs: {

        courseId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }
    },

    fn: async function (inputs, exits) {

        try {

            const { courseId } = inputs;

            const findOneCourse = await Course.findOne({
                where: { courseId: courseId }
            });

            if (!findOneCourse) {
                return exits.err('E_UNKNOWN_COURSE');
            }

            return exits.success(findOneCourse);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}