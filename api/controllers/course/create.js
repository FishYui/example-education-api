module.exports = {

    friendlyName: 'Create',

    description: 'Create one Course.',

    inputs: {

        name: {
            type: 'string',
            required: true
        },
        grade: {
            type: 'string',
            required: false
        },
        teacher: {
            type: 'string',
            required: true
        },
        department: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { name, grade, teacher, department } = inputs;

            const lastCourse = await Course.find({
                where: { courseId: { startsWith: `${department}` } },
                limit: 1,
                sort: 'id DESC',
                select: ['courseId']
            });


            let courseId;
            if (lastCourse.length === 0) {
                courseId = `${department}001`;
            } else {
                const lastCourseId = parseInt(lastCourse[0].courseId);
                courseId = (lastCourseId + 1).toString();
            }


            const courseData = {
                courseId,
                name,
                grade,
                teacher,
                department
            }

            const createCourse = await Course.create(courseData).fetch();

            return exits.success(createCourse);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}