module.exports = {

    friendlyName: 'Find',

    description: 'Find all Course.',

    inputs: {

        name: {
            type: 'string',
            required: false
        },
        grade: {
            type: 'string',
            required: false
        },
        teacher: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { name, grade, teacher, department } = inputs;

            const params = {
                name,
                grade,
                teacher,
                department
            }

            const findAllCourse = await Course.find(params);

            return exits.success(findAllCourse);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}