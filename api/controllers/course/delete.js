module.exports = {

    friendlyName: 'Delete',

    description: 'Delete one Course.',

    inputs: {

        courseId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { courseId } = inputs;

            const deleteCourse = await Course.destroyOne({
                where: { courseId: courseId }
            });

            if (!deleteCourse) {
                return exits.err('E_UNKNOWN_COURSE');
            }

            return exits.success();

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}