module.exports = {

    friendlyName: 'Update',

    description: 'Update one Department.',

    inputs: {

        departmentId: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { departmentId, name } = inputs;

            const departmentData = {
                departmentId,
                name
            }

            const updateDeaprtment = await Department.updateOne({ departmentId: departmentId }).set(departmentData);
            
            if(!updateDeaprtment){
                return exits.err('E_UNKNOWN_DEPARTMENT');
            }
            
            return exits.success(updateDeaprtment);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}