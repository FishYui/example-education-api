module.exports = {

    friendlyName: 'Delete',

    description: 'Delete one Department.',

    inputs: {

        departmentId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { departmentId } = inputs;

            const deleteDepartment = await Department.destroyOne({
                where: { departmentId: departmentId }
            });

            if(!deleteDepartment){
                return exits.err('E_UNKNOWN_DEPARTMENT');
            }

            return exits.success();

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}