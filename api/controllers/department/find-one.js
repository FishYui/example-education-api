module.exports = {

    friendlyName: 'Display',

    description: 'Display one Department.',

    inputs: {

        departmentId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { departmentId } = inputs;

            const findOneDepartment = await Department.findOne({
                where: { departmentId: departmentId }
            });
            
            if(!findOneDepartment){
                return exits.err('E_UNKNOWN_DEPARTMENT');
            }

            return exits.success(findOneDepartment);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}