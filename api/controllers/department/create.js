module.exports = {

    friendlyName: 'Create',

    description: 'Create one Department.',

    inputs: {

        departmentId: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { departmentId, name } = inputs;

            const departmentData = {
                departmentId,
                name
            }

            const createDeaprtment = await Department.create(departmentData).fetch();
            
            return exits.success(createDeaprtment);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}