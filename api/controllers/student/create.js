module.exports = {

    friendlyName: 'Create',

    description: 'Create one Student.',

    inputs: {

        name: {
            type: 'string',
            required: true
        },
        grade: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { name, grade, department } = inputs;


            //模擬學年，以新增當下的民國年、月份來計算
            const date = new Date();
            const year = date.getFullYear() - 1911;
            const month = date.getMonth();
            //月份小於7則算上一年度
            const school_year = month >= 7 ? year : year - 1;



            //找出當前學年、系所最後一位學號，將此學號數字加一即為新人的學號
            const lastStudent = await Student.find({
                where: { studentId: { startsWith: `${school_year}${department}` } },
                limit: 1,
                sort: 'id DESC',
                select: ['studentId']
            });


            
            //組合出學號
            let studentId;
            if(lastStudent.length === 0) { //找不到最後一位則新增001的學號
                studentId = `${school_year}${department}001`;
            }else {
                const lastStudentId = parseInt(lastStudent[0].studentId);
                studentId = (lastStudentId + 1).toString();
            }
            


            const studentData = {
                studentId,
                name,
                grade,
                school_year,
                department
            }

            const createStudent = await Student.create(studentData).fetch();

            return exits.success(createStudent);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}