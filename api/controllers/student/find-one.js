module.exports = {

    friendlyName: 'Find',

    description: 'Find one Student.',

    inputs: {

        studentId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { studentId } = inputs;

            const findOneStudent = await Student.findOne({
                where: { studentId: studentId }
            });
            
            if(!findOneStudent){
                return exits.err('E_UNKNOWN_STUDENT');
            }

            return exits.success(findOneStudent);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}