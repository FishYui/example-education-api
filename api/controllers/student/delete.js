module.exports = {

    friendlyName: 'Delete',

    description: 'Delete one Student.',

    inputs: {

        studentId: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { studentId } = inputs;

            const deleteStudent = await Student.destroyOne({
                where: { studentId: studentId }
            });

            if(!deleteStudent){
                return exits.err('E_UNKNOWN_STUDENT');
            }

            return exits.success();

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }

    }
}