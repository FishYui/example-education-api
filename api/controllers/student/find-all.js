module.exports = {

    friendly: 'Find',

    description: 'Find all Students.',

    inputs: {

        name: {
            type: 'string',
            required: false
        },
        grade: {
            type: 'string',
            required: false
        },
        school_year: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { name, grade, school_year, department } = inputs;

            const params = {
                name,
                grade, 
                school_year,
                department
            }

            const findAllStudent = await Student.find(params);

            return exits.success(findAllStudent);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }
    }
}