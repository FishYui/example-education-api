module.exports = {

    friendlyName: 'Update',

    description: 'Update one Student',

    inputs: {

        studentId: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: false
        },
        grade: {
            type: 'string',
            required: false
        },
        department: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { studentId, name, grade, department } = inputs;

            const studentData = {
                name,
                grade,
                department
            }

            const updateStudent = await Student.updateOne({
                where: { studentId: studentId },
            }).set(studentData);

            if (!updateStudent) {
                return exits.err('E_UNKNOWN_STUDENT');
            }

            return exits.success(updateStudent);

        } catch (err) {
            
            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr(err.message);
        }
    }
}