module.exports = {

    friendly: 'Select',

    description: 'Select one Course.',

    inputs: {

        student: {
            type: 'string',
            required: true
        },
        course: {
            type: 'string',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { student, course } = inputs;

            const electiveData = {
                student,
                course
            }

            const selectCourse = await Elective.create(electiveData);

            return exits.success(selectCourse);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}