module.exports = {

    friendlyName: 'UnSelect',

    description: 'UnSelect one Course.',

    inputs: {

        id: {
            type: 'string',
            required: false
        },
        student: {
            type: 'string',
            required: false
        },
        course: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { id, student, course } = inputs;
            let params;

            //判斷用選課資訊id還是學生id+課程id
            if (id) {
                params = id;
            } else if (student && course) {
                params = { student, course }
            } else {
                return exits.err('E_UNKNOWN_ELECTIVE');
            }

            const unSelect = await Elective.destroyOne(params);

            if (!unSelect) {
                return exits.err('E_UNKNOWN_ELECTIVE');
            }

            return exits.success();

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}