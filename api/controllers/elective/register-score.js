module.exports = {

    friendlyName: 'Register Score',

    description: 'Register one Score.',

    inputs: {

        id: {
            type: 'string',
            required: false
        },
        student: {
            type: 'string',
            required: false
        },
        course: {
            type: 'string',
            required: false
        },
        score: {
            type: 'number',
            required: true
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { id, student, course, score } = inputs;
            let params;

            //判斷用選課資訊id還是學生id+課程id
            if (id) {
                params = id;
            } else if (student && course) {
                params = { student, course }
            } else {
                return exits.err('E_UNKNOWN_ELECTIVE');
            }


            if (score > 100 || score < 0) {
                return exits.err('ER_WARN_DATA_OUT_OF_RANGE');
            }

            const scoreData = {
                score
            }

            
            const registerScore = await Elective.updateOne(params).set(scoreData);

            if (!registerScore) {
                return exits.err('E_UNKNOWN_ELECTIVE');
            }

            return exits.success(registerScore);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}