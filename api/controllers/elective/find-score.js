module.exports = {

    friendlyName: 'Find',

    description: 'Find Score.',

    inputs: {

        id: {
            type: 'string',
            required: false
        },
        student: {
            type: 'string',
            required: false
        },
        course: {
            type: 'string',
            required: false
        }

    },

    exits: {

        success: {
            responseType: 'ok'
        },
        err: {
            responseType: 'err'
        },
        serverErr: {
            responseType: 'serverErr'
        }

    },

    fn: async function (inputs, exits) {

        try {

            const { id, student, course } = inputs;
            let params;

            if (id) {
                params = id;
            } else if (student && course) {
                params = { student, course }
            } else if (student || course) {
                params = student ? { student } : { course };
            } else {
                params = {}
            }

            const findScore = await Elective.find(params);

            return exits.success(findScore);

        } catch (err) {

            const errCode = err.code || err.raw.code;

            if (errCode) {
                return exits.err(errCode);
            }

            return exits.serverErr();
        }
    }
}