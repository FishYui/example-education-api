# example-education-api

## Use
 - Language: Node.Js => Sails.js
 - DataBase: MySQL

## API
 - ### Department API
   - [**Create Department**](#create-department)
   - [**Find all Department**](#find-all-department) 
   - [**Find one Department**](#find-one-department)
   - [**Update one Department**](#update-one-department)
   - [**Delete one Department**](#delete-one-department)
 - ### Student API
   - [**Create Student**](#create-student) 
   - [**Find all Student**](#find-all-student)
   - [**Find one Student**](#find-one-student)
   - [**Update one Student**](#update-one-student)
   - [**Delete one Student**](#delete-one-student)
 - ### Teacher API
   - [**Create Teacher**](#create-teacher) 
   - [**Find all Teacher**](#find-all-teacher)
   - [**Find one Teacher**](#find-one-teacher)
   - [**Update one Teacher**](#update-one-teacher)
   - [**Delete one Teacher**](#delete-one-teacher)
 - ### Course API
   - [**Create Course**](#create-course) 
   - [**Find all Course**](#find-all-course)
   - [**Find one Course**](#find-one-course)
   - [**Update one Course**](#update-one-course)
   - [**Delete one Course**](#delete-one-course)
 - ### Elective API
   - [**Select one Course**](#select-one-course)
   - [**UnSelect one Course**](#unselect-one-course)
   - [**Register one Score**](#register-one-score)
   - [**Find Score**](#find-score) 

## Model
 - [**Department**](#department)
 - [**Student**](#student)
 - [**Teacher**](#teacher)
 - [**Course**](#course)
 - [**Elective**](#elective)

## Error Code
 - ### Public Use
   - [**E_UNIQUE**](#e_unique)
   - [**E_INVALID_NEW_RECORD**](#e_invalid_new_record)
   - [**ER_NO_REFERENCED_ROW_2**](#er_no_referenced_row_2)
   - [**ER_DATA_TOO_LONG**](#er_data_too_long)
   - [**ER_WARN_DATA_OUT_OF_RANGE**](#er_warn_data_out_of_range)
   - [**Server Error**](#server-error)
   - [**Other Error**](#other-error)
 - ### Department Use
   - [**E_UNKNOWN_DEPARTMENT**](#e_unknown_department)
 - ### Student Use
   - [**E_UNKNOWN_STUDENT**](#e_unknown_student)
 - ### Teacher Use
   - [**E_UNKNOWN_TEACHER**](#e_unknown_teacher)
 - ### Course Use
   - [**E_UNKNOWN_COURSE**](#e_unknown_course)
 - ### Elective Use
   - [**E_UNKNOWN_ELECTIVE**](#e_unknown_elective)

---

## Department API

 - ### Create Department

   `Create a new Department.`

   - **URL** : `/department`

   - **Method** : `POST`

   - **Parameters** : 

     - **departmentId**

       - **Type** : `string`

       - **Required** : `true`

     - **name**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Create a new Department, and data :`

       ```json
       {
           "departmentId": "999",
           "name": "Test_Department"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
               "createdAt": 1585226240225,
               "updatedAt": 1585226240225,
               "id": 11,
               "departmentId": "999",
               "name": "Test_Department"
           }
       }
       ```
    
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---

 - ### Find all Department

   `Find information for all Departments.`

   - **URL** : `/department`

   - **Method** : `GET`

   - **Parameters** : `None`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for all Departments.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585103353992,
                    "updatedAt": 1585109821264,
                    "id": 1,
                    "departmentId": "311",
                    "name": "醫資系"
                },
                {
                    "createdAt": 1585118225195,
                    "updatedAt": 1585118225195,
                    "id": 2,
                    "departmentId": "312",
                    "name": "醫學系"
                },
                {
                    "createdAt": 1585226240225,
                    "updatedAt": 1585226240225,
                    "id": 11,
                    "departmentId": "999",
                    "name": "Test_Department"
                }
            ]
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---

 - ### Find one Department

   `Find information for one Department.`

   - **URL** : `/department/:departmentId`

   - **Method** : `GET`

   - **Parameters** : 

     - **departmentId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for one Department with departmentId 311.`
       
       ```json
       {
           "success": true,
           "response": {
               "createdAt": 1585103353992,
               "updatedAt": 1585109821264,
               "id": 1,
               "departmentId": "311",
               "name": "醫資系"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Find information for Department with departmentId 111 :`
        [**E_UNKNOWN_DEPARTMENT**](#e_unknown_department)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---

 - ### Update one Department

   Update one Department's information.

   - **URL** : `/department/:departmentId`

   - **Method** : `PUT`

   - **Parameters** : 

     - **departmentId**

       - **Type** : `string`

       - **Required** : `true`
     
     - **name** 
       
       - **Type** : `string`

       - **Required** : `false`
   
   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Update one Department's information with departmentId 999, and data :`
       
       ```json
       {
           "name": "Test_Department123"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
               "createdAt": 1585226240225,
               "updatedAt": 1585227515455,
               "id": 11,
               "departmentId": "999",
               "name": "Test_Department123"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Update information for Department with departmentId 111 :`
        [**E_UNKNOWN_DEPARTMENT**](#e_unknown_department)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---

 - ### Delete one Department

   Delete one Department.

   - **URL** : `/department/:departmentId`

   - **Method** : `DELETE`

   - **Parameters** : 

     - **departmentId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Delete one Department with departmentId 999.`

       ```json
       {
           "departmentId": "999"
       }
       ```
       
       ```json
       {
           "success": true
       }
       ```
       
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Delete one Department with departmentId 999 twice :`
        [**E_UNKNOWN_DEPARTMENT**](#e_unknown_department)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)    
       
---

## Student API

 - ### Create Student

   `Create a new Student.`

   - **URL** : `/student`

   - **Method** : `POST`

   - **Parameters** : 

     - **name**

       - **Type** : `string`

       - **Required** : `true`

     - **grade**

       - **Type** : `string`

       - **Required** : `false`

     - **department**

       - **Type** : `string`

       - **Required** : `true`

       - **Reference** : `Department`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Create a new Student, and data :`

       ```json
       {
           "name": "Test_Sutdent",
           "department": "311"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
               "createdAt": 1585230015478,
               "updatedAt": 1585230015478,
               "id": 12,
               "studentId": "108311004",
               "name": "Test_Sutdent",
               "grade": "1",
               "school_year": 108,
               "department": "311"
           }
       }
       ```
    
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---

 - ### Find all Student

   `Find information for all Students.`

   - **URL** : `/student`

   - **Method** : `GET`

   - **Parameters** : 

     - **name**

       - **Type** : `string`

       - **Required** : `false`

     - **grade**

       - **Type** : `string`

       - **Required** : `false`
     
     - **school_year**

       - **Type** : `string`

       - **Required** : `false`

     - **department**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Department`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for all Students.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585209087161,
                    "updatedAt": 1585209087161,
                    "id": 5,
                    "studentId": "108311001",
                    "name": "醫資系學生1",
                    "grade": "1",
                    "school_year": 108,
                    "department": "311"
                },
                {
                    "createdAt": 1585209094106,
                    "updatedAt": 1585209094106,
                    "id": 6,
                    "studentId": "108311002",
                    "name": "醫資系學生2",
                    "grade": "1",
                    "school_year": 108,
                    "department": "311"
                },
                {
                    "createdAt": 1585209096410,
                    "updatedAt": 1585209096410,
                    "id": 7,
                    "studentId": "108311003",
                    "name": "醫資系學生3",
                    "grade": "1",
                    "school_year": 108,
                    "department": "311"
                }...etc
            ]
       }
       ```

       `Find information for all Students with department 312 and grade 2.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585209119700,
                    "updatedAt": 1585209119700,
                    "id": 10,
                    "studentId": "108312002",
                    "name": "醫學系學生2",
                    "grade": "2",
                    "school_year": 108,
                    "department": "312"
                },
                {
                    "createdAt": 1585209122630,
                    "updatedAt": 1585209122630,
                    "id": 11,
                    "studentId": "108312003",
                    "name": "醫學系學生3",
                    "grade": "2",
                    "school_year": 108,
                    "department": "312"
                }
            ]
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---
 
 - ### Find one Student

   `Find information for one Student.`

   - **URL** : `/student/:studentId`

   - **Method** : `GET`

   - **Parameters** : 

     - **studentId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for one Student with studentId 108311004.`
       
       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585230015478,
                "updatedAt": 1585230015478,
                "id": 12,
                "studentId": "108311004",
                "name": "Test_Sutdent",
                "grade": "1",
                "school_year": 108,
                "department": "311"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Find information for Student with studentId 111 :`
        [**E_UNKNOWN_STUDENT**](#e_unknown_student)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---
 
 - ### Update one Student

   Update one Student's information.

   - **URL** : `/student/:studentId`

   - **Method** : `PUT`

   - **Parameters** : 

     - **studentId**

       - **Type** : `string`

       - **Required** : `true`
     
     - **name** 
       
       - **Type** : `string`

       - **Required** : `false`

     - **grade** 
       
       - **Type** : `string`

       - **Required** : `false`

     - **department** 
       
       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Department`
   
   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Update one Student's information with studentId 108311004, and data :`
       
       ```json
       {
           "name": "Test_Sutdent123",
           "grade": "1",
           "department": "312"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585230015478,
                "updatedAt": 1585230860067,
                "id": 12,
                "studentId": "108311004",
                "name": "Test_Sutdent123",
                "grade": "1",
                "school_year": 108,
                "department": "312"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Update information for Student with studentId 111 :`
        [**E_UNKNOWN_STUDENT**](#e_unknown_student)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---
 
 - ### Delete one Student

   Delete one Student.

   - **URL** : `/student/:studentId`

   - **Method** : `DELETE`

   - **Parameters** : 

     - **studentId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Delete one Student with studentId 108311004.`

       ```json
       {
           "studentId": "108311004"
       }
       ``` 

       ```json
       {
           "success": true
       }
       ```
       
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Delete one Student with studentId 108311004 twice :`
        [**E_UNKNOWN_STUDENT**](#e_unknown_student)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)    

---

## Teacher API

 - ### Create Teacher

   `Create a new Teacher.`

   - **URL** : `/teacher`

   - **Method** : `POST`

   - **Parameters** : 

     - **name**

       - **Type** : `string`

       - **Required** : `true`

     - **department**

       - **Type** : `string`

       - **Required** : `true`

       - **Reference** : `Department`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Create a new Teacher, and data :`

       ```json
       {
           "name": "Test_Teacher",
           "department": "311"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585231262045,
                "updatedAt": 1585231262045,
                "id": 15,
                "teacherId": "108311904",
                "name": "Test_Teacher",
                "school_year": 108,
                "department": "311"
           }
       }
       ```
    
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---
 
 - ### Find all Teacher

   `Find information for all Teachers.`

   - **URL** : `/teacher`

   - **Method** : `GET`

   - **Parameters** : 

     - **name**

       - **Type** : `string`

       - **Required** : `false`

     - **school_year**

       - **Type** : `string`

       - **Required** : `false`

     - **department**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Department`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for all Teachers.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585186028061,
                    "updatedAt": 1585186028061,
                    "id": 7,
                    "teacherId": "108311901",
                    "name": "醫資系教師1",
                    "school_year": 108,
                    "department": "311"
                },
                {
                    "createdAt": 1585186032266,
                    "updatedAt": 1585186032266,
                    "id": 8,
                    "teacherId": "108311902",
                    "name": "醫資系教師2",
                    "school_year": 108,
                    "department": "311"
                },
                {
                    "createdAt": 1585186035105,
                    "updatedAt": 1585186035105,
                    "id": 9,
                    "teacherId": "108311903",
                    "name": "醫資系教師3",
                    "school_year": 108,
                    "department": "311"
                }...etc
            ]
       }
       ```

       `Find information for all Teachers with department 312 and school_year 108.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585186050330,
                    "updatedAt": 1585186050330,
                    "id": 10,
                    "teacherId": "108312901",
                    "name": "醫學系教師1",
                    "school_year": 108,
                    "department": "312"
                },
                {
                    "createdAt": 1585186052892,
                    "updatedAt": 1585186052892,
                    "id": 11,
                    "teacherId": "108312902",
                    "name": "醫學系教師2",
                    "school_year": 108,
                    "department": "312"
                },
                {
                    "createdAt": 1585186055739,
                    "updatedAt": 1585186055739,
                    "id": 12,
                    "teacherId": "108312903",
                    "name": "醫學系教師3",
                    "school_year": 108,
                    "department": "312"
                }
            ]
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---
 
 - ### Find one Teacher

   `Find information for one Teacher.`

   - **URL** : `/teacher/:teacherId`

   - **Method** : `GET`

   - **Parameters** : 

     - **teacherId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for one Teacher with teacherId 108311904.`
       
       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585231262045,
                "updatedAt": 1585231262045,
                "id": 15,
                "teacherId": "108311904",
                "name": "Test_Teacher",
                "school_year": 108,
                "department": "311"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Find information for Teacher with teacherId 111 :`
        [**E_UNKNOWN_TEACHER**](#e_unknown_teacher)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---
 
 - ### Update one Teacher

   Update one Teacher's information.

   - **URL** : `/teacher/:teacherId`

   - **Method** : `PUT`

   - **Parameters** : 

     - **teacherId**

       - **Type** : `string`

       - **Required** : `true`
     
     - **name** 
       
       - **Type** : `string`

       - **Required** : `false`

     - **department** 
       
       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Department`
   
   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Update one Teacher's information with teacherId 108311904, and data :`
       
       ```json
       {
           "name": "Test_Teacher123",
           "department": "312"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585231262045,
                "updatedAt": 1585231672718,
                "id": 15,
                "teacherId": "108311904",
                "name": "Test_Teacher123",
                "school_year": 108,
                "department": "312"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Update information for Teacher with teacherId 111 :`
        [**E_UNKNOWN_TEACHER**](#e_unknown_teacher)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---
 
 - ### Delete one Teacher

   Delete one Teacher.

   - **URL** : `/teacher/:teacherId`

   - **Method** : `DELETE`

   - **Parameters** : 

     - **teacherId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Delete one Teacher with teacherId 108311904.`

       ```json
       {
           "teacherId": "108311904"
       }
       ``` 

       ```json
       {
           "success": true
       }
       ```
       
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Delete one Teacher with teacherId 108311904 twice :`
        [**E_UNKNOWN_TEACHER**](#e_unknown_teacher)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)    

---

## Course API
 
 - ### Create Course

   `Create a new Course.`

   - **URL** : `/course`

   - **Method** : `POST`

   - **Parameters** : 

     - **name**

       - **Type** : `string`

       - **Required** : `true`

     - **grade**

       - **Type** : `string`

       - **Required** : `false`

     - **teacher**

       - **Type** : `string`

       - **Required** : `true`

       - **Reference** : `Teacher`

     - **department**

       - **Type** : `string`

       - **Required** : `true`

       - **Reference** : `Department`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Create a new Course, and data :`

       ```json
       {
           "name": "Test_Course",
           "teacher": "108311901",
           "department": "311"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585232150734,
                "updatedAt": 1585232150734,
                "id": 18,
                "courseId": "311007",
                "name": "Test_Course",
                "grade": "1",
                "teacher": "108311901",
                "department": "311"
           }
       }
       ```
    
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---
 
 - ### Find all Course

   `Find information for all Courses.`

   - **URL** : `/course`

   - **Method** : `GET`

   - **Parameters** : 

     - **name**

       - **Type** : `string`

       - **Required** : `false`

     - **grade**

       - **Type** : `string`

       - **Required** : `false`

     - **teacher**

       - **Type** : `string`

       - **Required** : `false`
       
       - **Reference** : `Teacher` 

     - **department**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Department`


   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for all Courses.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585199263583,
                    "updatedAt": 1585199263583,
                    "id": 5,
                    "courseId": "311001",
                    "name": "醫資系課程1",
                    "grade": "1",
                    "teacher": "108311901",
                    "department": "311"
                },
                {
                    "createdAt": 1585199267254,
                    "updatedAt": 1585199267254,
                    "id": 6,
                    "courseId": "311002",
                    "name": "醫資系課程2",
                    "grade": "1",
                    "teacher": "108311901",
                    "department": "311"
                },
                {
                    "createdAt": 1585199271838,
                    "updatedAt": 1585199271838,
                    "id": 7,
                    "courseId": "311003",
                    "name": "醫資系課程3",
                    "grade": "1",
                    "teacher": "108311902",
                    "department": "311"
                }...etc
            ]
       }
       ```

       `Find information for all Courses with department 312 and grade 3.`

       ```json
       {
           "success": true,
           "response": [
                {
                    "createdAt": 1585199328815,
                    "updatedAt": 1585200866925,
                    "id": 15,
                    "courseId": "312005",
                    "name": "醫學系課程5",
                    "grade": "3",
                    "teacher": "108312903",
                    "department": "312"
                },
                {
                    "createdAt": 1585199331817,
                    "updatedAt": 1585200857899,
                    "id": 16,
                    "courseId": "312006",
                    "name": "醫學系課程6",
                    "grade": "3",
                    "teacher": "108312903",
                    "department": "312"
                }
            ]
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---
 
 - ### Find one Course

   `Find information for one Course.`

   - **URL** : `/course/:courseId`

   - **Method** : `GET`

   - **Parameters** : 

     - **courseId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find information for one Course with courseId 311007.`
       
       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585232150734,
                "updatedAt": 1585232150734,
                "id": 18,
                "courseId": "311007",
                "name": "Test_Course",
                "grade": "1",
                "teacher": "108311901",
                "department": "311"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Find information for Course with courseId 111 :`
        [**E_UNKNOWN_COURSE**](#e_unknown_course)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---

 - ### Update one Course

   Update one Course's information.

   - **URL** : `/course/:courseId`

   - **Method** : `PUT`

   - **Parameters** : 

     - **courseId**

       - **Type** : `string`

       - **Required** : `true`
     
     - **name** 
       
       - **Type** : `string`

       - **Required** : `false`

     - **grade** 
       
       - **Type** : `string`

       - **Required** : `false`

     - **teacher** 
       
       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Teacher`

     - **department** 
       
       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Department`
   
   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Update one Course's information with courseId 311007, and data :`
       
       ```json
       {
           "name": "Test_Course123",
           "grade": "4",
           "teacher": "108311902",
           "department": "312"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585232150734,
                "updatedAt": 1585233009553,
                "id": 18,
                "courseId": "311007",
                "name": "Test_Course123",
                "grade": "4",
                "teacher": "108311902",
                "department": "312"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Update information for Course with courseId 111 :`
        [**E_UNKNOWN_COURSE**](#e_unknown_course)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---
 
 - ### Delete one Course

   Delete one Course.

   - **URL** : `/course/:courseId`

   - **Method** : `DELETE`

   - **Parameters** : 

     - **courseId**

       - **Type** : `string`

       - **Required** : `true`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Delete one Course with courseId 311007.`

       ```json
       {
           "courseId": "311007"
       }
       ``` 

       ```json
       {
           "success": true
       }
       ```
       
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Delete one Course with courseId 311007 twice :`
        [**E_UNKNOWN_COURSE**](#e_unknown_course)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)    

---

## Elective API
 
 - ### Select one Course

   `Select one Course.`

   - **URL** : `/select`

   - **Method** : `POST`

   - **Parameters** : 

     - **student**

       - **Type** : `string`

       - **Required** : `true`

       - **Reference** : `Student`

     - **course**

       - **Type** : `string`

       - **Required** : `true`

       - **Reference** : `Course`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Select one Course, and data :`

       ```json
       {
           "student": "108312003",
	       "course": "312004"
       }
       ```

       ```json
       {
           "success": true,
       }
       ```
    
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---
 
 - ### UnSelect one Course

   UnSelect one Course.

   - **URL** : `/unselect/:id?`

   - **Method** : `DELETE`

   - **Parameters** : 

     - **id**

       - **Type** : `string`

       - **Required** : `false`

     - **student**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Student`

     - **course**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Course`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Student UnSelect one Course with id 18.`

       ```json
       {
           "id": "18"
       }
       ```
       
       ```json
       {
           "success": true
       }
       ```
       
       `Student UnSelect one Course with studentId 108312003 and courseId 311007.`

       ```json
       {
           "student": "108312003",
	       "course": "312004"
       }
       ```
       
       ```json
       {
           "success": true
       }
       ```
       
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `UnSelect one Course with twice :`
        [**E_UNKNOWN_ELECTIVE**](#e_unknown_elective)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)    

---

 
 - ### Register one Score

   Register one Score.

   - **URL** : `/score/:id?`

   - **Method** : `PUT`

   - **Parameters** : 

     - **id**

       - **Type** : `string`

       - **Required** : `false`

     - **student**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Student`

     - **course**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Course`
     
     - **score**

       - **Type** : `number`

       - **Required** : `true`
   
   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Register one Score with id 19, and data :`
       
       ```json
       {
           "score": "80"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585234542355,
                "updatedAt": 1585234590956,
                "id": 19,
                "score": 80,
                "student": "108312003",
                "course": "312004"
           }
       }
       ```

       `Register one Score with student 108312003 and course 312004, and data :`
       
       ```json
       {
           "score": "100"
       }
       ```

       ```json
       {
           "success": true,
           "response": {
                "createdAt": 1585234542355,
                "updatedAt": 1585234743871,
                "id": 19,
                "score": 100,
                "student": "108312003",
                "course": "312004"
           }
       }
       ```

   - **Error Response:**

      - **Code** : `400`
      - **Content Example** :

        `Register Score without id or (student and course) :`
        [**E_UNKNOWN_ELECTIVE**](#e_unknown_elective)

        `Register Score and score 1000 :`
        [**ER_WARN_DATA_OUT_OF_RANGE**](#er_warn_data_out_of_range)

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error) 

---
 
 - ### Find Score

   `Find Score.`

   - **URL** : `/score/:id?`

   - **Method** : `GET`

   - **Parameters** : 

     - **id**

       - **Type** : `string`

       - **Required** : `false`
     
     - **student**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Student`

     - **course**

       - **Type** : `string`

       - **Required** : `false`

       - **Reference** : `Course`

   - **Success Response** : 

     - **Code** : `200`

     - **Content Example** : 
     
       `Find Score and no Parameters :`

       ```json
       {
           "success": true,
           "response": [
               {
                   "createdAt": 1585210696723,
                   "updatedAt": 1585212898290,
                   "id": 8,
                   "score": 100,
                   "student": "108311001",
                   "course": "311001"
               },
               {
                   "createdAt": 1585212424738,
                   "updatedAt": 1585212424738,
                   "id": 9,
                   "score": 0,
                   "student": "108311002",
                   "course": "311001"
               },
               {
                   "createdAt": 1585212433118,
                   "updatedAt": 1585212433118,
                   "id": 11,
                   "score": 0,
                   "student": "108311003",
                   "course": "311001"
               }...etc
           ]
       }
       ```

       `Find Score with student 108311001 :`

       ```json
       {
           "success": true,
           "response": [
               {
                   "createdAt": 1585210696723,
                   "updatedAt": 1585212898290,
                   "id": 8,
                   "score": 100,
                   "student": "108311001",
                   "course": "311001"
               },
               {
                   "createdAt": 1585212465310,
                   "updatedAt": 1585212465310,
                   "id": 12,
                   "score": 0,
                   "student": "108311001",
                   "course": "311002"
               },
               {
                   "createdAt": 1585212469100,
                   "updatedAt": 1585212469100,
                   "id": 13,
                   "score": 0,
                   "student": "108311001",
                   "course": "311003"
               }
           ]
       }
       ```
    
   - **Error Response:**

      - **Code** : `400`
      - **Content Example** : [**Other Error**](#other-error)

      - **Code** : `500`
      - **Content Example** : [**Server Error**](#server-error)

---

# Model

 - ### Department
   - **createdAt**
     - `Type: BIGINT`
     - `AllowNull: false`
   - **updatedAt**
     - `Type: BIGINT`
     - `AllowNull: false` 
   - **id**
     - `Type: INT`
     - `AllowNull: false` 
     - `Primary Key`
     - `Unique Key`
     - `Auto Increment`
   - **departmentId**
     - `Type: CHAR (3)`
     - `AllowNull: false` 
     - `Unique Key`
   - **name**
     - `Type: CHAR (20)`
     - `AllowNull: false` 

 - ### Student
   - **createdAt**
     - `Type: BIGINT`
     - `AllowNull: false`
   - **updatedAt**
     - `Type: BIGINT`
     - `AllowNull: false` 
   - **id**
     - `Type: INT`
     - `AllowNull: false` 
     - `Primary Key`
     - `Unique Key`
     - `Auto Increment`
   - **studentId**
     - `Type: CHAR (9)`
     - `AllowNull: false` 
     - `Unique Key`
   - **name**
     - `Type: CHAR (20)`
     - `AllowNull: false`
   - **grade**
     - `Type: CHAR (2)`
     - `INIT ['1','2','3','4','5','6','7','8','9']`
     - `AllowNull: false`
   - **school_year**
     - `Type: TINYINT`
     - `AllowNull: false`
   - **department**
     - `Type: CHAR (3)`
     - `AllowNull: false` 
     - `Foreign Key reference department(departmentId) ON UPDATE CASCADE ON DELETE CASCADE`

 - ### Teacher
   - **createdAt**
     - `Type: BIGINT`
     - `AllowNull: false`
   - **updatedAt**
     - `Type: BIGINT`
     - `AllowNull: false` 
   - **id**
     - `Type: INT`
     - `AllowNull: false` 
     - `Primary Key`
     - `Unique Key`
     - `Auto Increment`
   - **teacherId**
     - `Type: CHAR (9)`
     - `AllowNull: false` 
     - `Unique Key`
   - **name**
     - `Type: CHAR (20)`
     - `AllowNull: false`
   - **school_year**
     - `Type: TINYINT`
     - `AllowNull: false`
   - **department**
     - `Type: CHAR (3)`
     - `AllowNull: false` 
     - `Foreign Key reference department(departmentId) ON UPDATE CASCADE ON DELETE CASCADE` 
 
 - ### Course
   - **createdAt**
     - `Type: BIGINT`
     - `AllowNull: false`
   - **updatedAt**
     - `Type: BIGINT`
     - `AllowNull: false` 
   - **id**
     - `Type: INT`
     - `AllowNull: false` 
     - `Primary Key`
     - `Unique Key`
     - `Auto Increment`
   - **courseId**
     - `Type: CHAR (9)`
     - `AllowNull: false` 
     - `Unique Key`
   - **name**
     - `Type: CHAR (20)`
     - `AllowNull: false`
   - **grade**
     - `Type: CHAR (2)`
     - `INIT ['1','2','3','4','5','6','7','8','9']`
     - `AllowNull: false`
   - **teacher**
     - `Type: CHAR (9)`
     - `AllowNull: false` 
     - `Foreign Key reference teacher(teacherId) ON UPDATE CASCADE ON DELETE CASCADE`
   - **department**
     - `Type: CHAR (3)`
     - `AllowNull: false` 
     - `Foreign Key reference department(departmentId) ON UPDATE CASCADE ON DELETE CASCADE` 

 - ### Elective
   - **createdAt**
     - `Type: BIGINT`
     - `AllowNull: false`
   - **updatedAt**
     - `Type: BIGINT`
     - `AllowNull: false` 
   - **id**
     - `Type: INT`
     - `AllowNull: false` 
     - `Primary Key`
     - `Auto Increment`
   - **score**
     - `Type: TINYINT`
     - `AllowNull: false`
   - **student**
     - `Type: CHAR (9)`
     - `AllowNull: false` 
     - `Foreign Key reference student(studentId) ON UPDATE CASCADE ON DELETE CASCADE`
   - **course**
     - `Type: CHAR (3)`
     - `AllowNull: false` 
     - `Foreign Key reference course(courseId) ON UPDATE CASCADE ON DELETE CASCADE`

---

# Error Code
 
 ### Public  

  - #### E_UNIQUE
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: ID重複"
    }
    ```
  - #### E_INVALID_NEW_RECORD
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 未填寫正確"
    }
    ```
  - #### ER_NO_REFERENCED_ROW_2
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 關聯的外鍵不存在"
    }
    ```
  - #### ER_DATA_TOO_LONG
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 資料輸入過長"
    }
    ```
  - #### ER_WARN_DATA_OUT_OF_RANGE
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 資料輸入超出範圍" 
    }
    ```
  - #### Server Error
    ```json
    {
        "status": 500,
        "message": #error.message
    }
    ```
  - #### Other Error
    ```json
    {
        "status": 400,
        "message": "Other Error"
    }
    ```

 ### Department Use

  - #### E_UNKNOWN_DEPARTMENT
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 找不到系所"
    }
    ```

 ### Student Use 

  - #### E_UNKNOWN_STUDENT
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 找不到學生"
    }
    ```

 ### Teacher Use

  - #### E_UNKNOWN_TEACHER
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 找不到教師"
    }
    ```

 ### Course Use

  - #### E_UNKNOWN_COURSE
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 找不到課程" 
    }
    ```

 ### Elective Use

  - #### E_UNKNOWN_ELECTIVE
    ```json
    {
        "status": 400,
        "message": "輸入錯誤: 找不到選課資訊"
    }
    ```
