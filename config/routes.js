/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

  //系所管理
  'POST /department': 'department/create',
  'GET /department': 'department/find-all',
  'GET /department/:departmentId': 'department/find-one',
  'PUT /department/:departmentId': 'department/update',
  'DELETE /department/:departmentId': 'department/delete',

  //學生相關
  'POST /student': 'student/create',
  'GET /student': 'student/find-all',
  'GET /student/:studentId': 'student/find-one',
  'PUT /student/:studentId': 'student/update',
  'DELETE /student/:studentId': 'student/delete',

  //教師相關
  'POST /teacher': 'teacher/create',
  'GET /teacher': 'teacher/find-all',
  'GET /teacher/:teacherId': 'teacher/find-one',
  'PUT /teacher/:teacherId': 'teacher/update',
  'DELETE /teacher/:teacherId': 'teacher/delete',

  //課程相關
  'POST /course': 'course/create',
  'GET /course': 'course/find-all',
  'GET /course/:courseId': 'course/find-one',
  'PUT /course/:courseId': 'course/update',
  'DELETE /course/:courseId': 'course/delete',

  //選課相關
  'POST /select': 'elective/select',
  'DELETE /unselect/:id?': 'elective/unselect',
  'PUT /score/:id?': 'elective/register-score',
  'GET /score/:id?': 'elective/find-score',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
