module.exports.errcode = {
    code: {
        
        'E_UNIQUE': {
            status: 400,
            message: '輸入錯誤: ID重複'
        },
        'E_INVALID_NEW_RECORD': {
            status: 400,
            message: '輸入錯誤: 未填寫正確'
        },
        'ER_NO_REFERENCED_ROW_2': {
            status: 400,
            message: '輸入錯誤: 關聯的外鍵不存在'
        },
        'ER_DATA_TOO_LONG': {
            status: 400,
            message: '輸入錯誤: 資料輸入過長'
        },
        'ER_WARN_DATA_OUT_OF_RANGE': {
            status: 400,
            message: '輸入錯誤: 資料輸入超出範圍'
        },


        //系所相關
        'E_UNKNOWN_DEPARTMENT': {
            status: 400,
            message: '輸入錯誤: 找不到系所'
        },

        //學生相關
        'E_UNKNOWN_STUDENT': {
            status: 400,
            message: '輸入錯誤: 找不到學生'
        },

        //教師相關
        'E_UNKNOWN_TEACHER': {
            status: 400,
            message: '輸入錯誤: 找不到教師'
        },

        //課程相關
        'E_UNKNOWN_COURSE': {
            status: 400,
            message: '輸入錯誤: 找不到課程'
        },

        //選課相關
        'E_UNKNOWN_ELECTIVE': {
            status: 400,
            message: '輸入錯誤: 找不到選課資訊'
        }

    }
}